## recompiling tex file
cd raw
for chap in Kapitel*/
do
	cd "$chap"
	for VL in VL_*.tex
	do
		pdflatex $VL
		mv "$(basename $VL .tex).pdf" ../../pdf/
	done
	cd ..
done

#cd MafIA_II_Komplett/
#pdflatex *.tex
#pdflatex *.tex
#mv *.pdf ../../pdf/

cd ..

## generating html
cat ../html/header.html > index.html
cat ../html/body_start.html >> index.html
echo "<h3>Stand: $(date +%d.%m.%Y)</h3>" >> index.html

for pdf in pdf/*pdf; do
	echo "<li><a href=\"$pdf\">$(basename "$pdf" .pdf)</a></li>" >> index.html
done

cat ../html/body_end.html >> index.html
