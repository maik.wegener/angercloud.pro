Repo für LaTeX-Mitschriften verschiedener Module

## Diskrete Mathematik
Kompilierte Mitschriften der Veranstaltung "Diskrete Mathematik" aus dem Wintersemester 2017/18 finden sich hier:
https://uni.angercloud.pro/Diskrete%20Mathematik/index.html

## Diskrete Stochastik
Die Mitschrift für DiSto entsteht gerade noch und ein aktueller Stand wird bei Zeiten hier verlinkt.
