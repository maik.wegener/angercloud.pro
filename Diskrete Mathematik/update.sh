## recompiling tex file
cd raw
for chap in Kapitel*/
    do 
        cd $chap
        pdflatex Kapitel*.tex
        mv Kapitel*.pdf ../../pdf/
        cd ..
done

cd Diskrete_Mathematik_Komplett/
pdflatex *.tex
pdflatex *.tex
mv *.pdf ../../pdf/

cd ../..

## generating html
cat ../html/header.html > index.html
cat ../html/body_start.html >> index.html
echo "<h3>Stand: $(date +%d.%m.%Y)</h3>" >> index.html

for pdf in pdf/*pdf; do
	echo "<li><a href=\"$pdf\">$(basename "$pdf" .pdf)</a></li>" >> index.html
done
echo "<li><a href=pdf/scanned/>Handschriftliche Dokumente (alt)</a></li>" >> index.html

cat ../html/body_end.html >> index.html
