## checking for changes and recompiling latex if necessary
if git log --name-only | grep -q 'Diskrete Mathematik/'; then
	cd 'Diskrete Mathematik'
#	sh update.sh
	cd ..
fi
if git log --name-only | grep -q 'Mathematik für Informatik Anfänger II/'; then
	cd 'Mathematik für Informatik Anfänger II'
	sh update.sh
	cd ..
fi
if git log --name-only | grep -q 'Neurobiology/'; then
	cd 'Neurobiology'
#	sh update.sh
	cd ..
fi
if git log --name-only | grep -q 'Allgemeines Programmierpraktikum'; then
	cd 'Allgemeines Programmierpraktikum'
#	sh update.sh
	cd ..
fi
if git log --name-only | grep -q 'Theoretische Neurowissenschaften - Grundlagen'/; then
	cd 'Theoretische Neurowissenschaften - Grundlagen'
#	sh update.sh
	cd ..
fi


## generating html
cat html/header.html > index.html
cat html/body_start.html >> index.html
echo "<h3>Stand: $(date +%d.%m.%Y)</h3>" >> index.html

for mod in */index.html; do
	echo "<li><a href=\"$mod\">$(dirname "$mod")</a></li>" >> index.html
done

cat html/body_end.html >> index.html

